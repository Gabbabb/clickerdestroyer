﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace ClickerDestroyer
{
    class Ball
    {
        //private float ySpeed;
        //private float xSpeed;
        private Vector2 position;
        private Vector2 speed;
        public bool destroyed = false;

        public Vector2 Position { get => position; }
        public Ball(float x, float y, float ySpeed, float xSpeed)
        {
            //this.ySpeed = RepairZeroSpeed(ySpeed);
            //this.speed.X = RepairZeroSpeed(xSpeed);
            speed.Y = RepairZeroSpeed(ySpeed);
            speed.X = RepairZeroSpeed(xSpeed);
            this.position = new Vector2(x, y);
        }
        public void Move()
        {
            bool flewOutByY = position.Y == 0 || position.Y == GlobalGameVals.LimitYpos();
            bool flewOutByX = position.X == 0 || position.X == GlobalGameVals.LimitXpos();
            if (flewOutByY)
                speed.Y = HitBorder(speed.Y);
            if (flewOutByX)
                speed.X = HitBorder(speed.X);
            if (flewOutByY | flewOutByY)
            {
                speed.X = SlowDown(speed.X);
                speed.Y = SlowDown(speed.Y);
            } 
            ChangePos();
        }
        public void ChangeDir()
        {
            RandomChangeDir();
            speed.X = SpeedUp(speed.X);
            speed.Y = SpeedUp(speed.Y);
        }
        public void Destroy()
        {
            destroyed = true;
        }

        private float SpeedUp(float speed)
        { 
            return speed + Math.Sign(speed) * GlobalGameVals.SPEEDUP_RATE; ;
        }
        private float SlowDown(float speed)
        {
            if (speed < -1 || speed > 1)
                speed -= Math.Sign(speed) * GlobalGameVals.SLOWDOWN_RATE;
            return speed;
        }
        private float HitBorder(float speed)
        {
            return -speed;
        }
        // координаты никогда не будут установлены за пределами игрового окна
        private float CalcNewPos(float coordinate, float speed, float limit)
        {
            coordinate += speed;
            if (coordinate > limit)
                return limit;
            if (coordinate < 0)
                return 0;
            return coordinate;
        }                
        private void ChangePos()
        {
            position.Y = CalcNewPos(position.Y, speed.Y, GlobalGameVals.LimitYpos());
            position.X = CalcNewPos(position.X, speed.X, GlobalGameVals.LimitXpos());
        }
        // нельзя установить скорость == 0, значение по умолчанию == 1
        private float RepairZeroSpeed(float speed)
        {
            if (speed == 0)
                return 1;
            else
                return speed;
        }        
        private void RandomChangeDir()
        {
            Random random = new Random();
            double angle =  Math.PI * (double)random.Next(0, 359) / 180;
            speed = Vector2.Transform(speed, Matrix.CreateRotationZ((float)angle));
            /*float sin = (float)Math.Sin(angle);
            float cos = (float)Math.Cos(angle);
            xSpeed = xSpeed * cos - ySpeed * sin;
            ySpeed = xSpeed * sin + ySpeed * cos;*/
        }
        /*
        public void HitEffect(float cursorPositionX, float cursorPositionY)
        {
            Vector2 cursorPosition = new Vector2(cursorPositionX, cursorPositionY);
            ChangeDirBy(cursorPosition);
            xSpeed = SpeedUp(xSpeed);
            ySpeed = SpeedUp(ySpeed);
        }
        private void ChangeDirBy(Vector2 cursorPosition)
        {            
            // (cursorPosition - position) это вектор, определяющий положение курсора в системе координат с точкой отчёта в центре круга
            // -(cursorPosition - position) симметричный ему относительно точки отчёта
            Vector2 newDir = -(cursorPosition - GetCenter());
            double newAngle = Math.Atan(newDir.Y / newDir.X);
            speed.X = xSpeed;
            speed.Y = ySpeed;
            double angle = Math.Atan(speed.Y / speed.X);
            double deltaAngle = angle - newAngle;
            speed = Vector2.Transform(speed, Matrix.CreateRotationZ((float)deltaAngle));
            xSpeed = speed.X;
            ySpeed = speed.Y;
        }
        private Vector2 GetCenter()
        {
            return new Vector2(position.X + GlobalGameVals.BALL_WIDTH/2, position.Y + GlobalGameVals.BALL_HEIGHT / 2);
        }
        */
    }
}