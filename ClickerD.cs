﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;

namespace ClickerDestroyer
{
    public class Score
    {
        public ushort hit;
        public ushort miss;
    }

    /// <summary>
    /// This is the main type for your game.
    /// </summary>   
    public class ClickerD : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        // текстуры
        private Texture2D texBall;
        private SpriteFont scoreFont;
        // модель
        private Ball ball;
        private List<Ball> balls;
        // счёт
        private Score player;
        private Vector2 scorePos;
        private string score;
        // счётчик изменяется, если левая кнопка мыши была зажата и отпущена за то время, пока курсор указывал на шар
        private bool just_pressed = false;
        private bool gameOver = false;

        private TextOnScreen scoreText;
        private TextOnScreen gameOverText;
        private TextOnScreen playAgainText;
        private TextOnScreen exitText;

        private double elapsedTime;

        public ClickerD()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            // window size
            base.Initialize();
            this.IsMouseVisible = true;

            graphics.PreferredBackBufferWidth = GlobalGameVals.WIDTH;
            graphics.PreferredBackBufferHeight = GlobalGameVals.HEIGHT;
            graphics.ApplyChanges();

            if (GlobalGameVals.GAME_MODE == GameMode.Chase)
            {
                player = new Score();
                Reset();
                InitText();
            }
            
            scorePos = new Vector2(ContentConf.SCORE_X, ContentConf.SCORE_Y);            
           
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            // ball's texture
            texBall = Content.Load<Texture2D>("ball3");
            GlobalGameVals.BALL_HEIGHT = texBall.Height;
            GlobalGameVals.BALL_WIDTH = texBall.Width;
            // font
            scoreFont = Content.Load<SpriteFont>("count");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GlobalGameVals.GAME_MODE == GameMode.Chase)
                TheChase(gameTime);
            if (GlobalGameVals.GAME_MODE == GameMode.Hunt)
                TheHunt(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(new Color(0,130,209));

            spriteBatch.Begin();
            if (GlobalGameVals.GAME_MODE == GameMode.Chase)
            {
                spriteBatch.Draw(texBall, ball.Position, ContentConf.FONT_COLOR);
                spriteBatch.DrawString(scoreFont, score, scorePos, ContentConf.FONT_COLOR);
                if (gameOver)
                {
                    //spriteBatch.DrawString(scoreFont, gameOverText.text, gameOverText.pos, Color.OrangeRed);
                    DrawOutlinedString(gameOverText, Color.Red);
                    DrawOutlinedString(playAgainText, GetButtonColor(playAgainText));
                    DrawOutlinedString(exitText, GetButtonColor(exitText));
                }
                else
                {
                    double time = Math.Truncate(GlobalGameVals.TIME_LIMIT) - Math.Truncate(elapsedTime);
                    spriteBatch.DrawString(scoreFont, time.ToString(), new Vector2(300, 10), Color.White);
                }
            }           
                
            spriteBatch.End();

            base.Draw(gameTime);
        }

        private bool CursorInArea(Vector2 pos, float length, float height)
        {
            MouseState currMousePos = Mouse.GetState();
            return (currMousePos.X > pos.X &&
            currMousePos.X < pos.X + length &&
            currMousePos.Y > pos.Y &&
            currMousePos.Y < pos.Y + height);
        }
        private bool CursorOnBall()
        {
            return CursorInArea(ball.Position, GlobalGameVals.BALL_WIDTH, GlobalGameVals.BALL_HEIGHT);
        }
        private bool CursorOnText(TextOnScreen text)
        {
            return CursorInArea(text.pos, text.width, text.hight);
        }
        private void ConfirmHit(Ball ball)
        {

            //MouseState currMousePos = Mouse.GetState();
            if (GlobalGameVals.GAME_MODE == GameMode.Chase)
            {
                player.hit++;
                ball.ChangeDir();
                elapsedTime -= GlobalGameVals.TIME_REWARD;
            }
            if (GlobalGameVals.GAME_MODE == GameMode.Hunt)
                ball.Destroy();

        }
        private Ball SpawnBall()
        {
            Random random = new Random();
            int x = random.Next(0, GlobalGameVals.LimitXpos());
            int y = random.Next(0, GlobalGameVals.LimitYpos());
            float xSpeed = random.Next(-5, 5);
            float ySpeed = random.Next(-5, 5);
            return new Ball(x, y, ySpeed, xSpeed);
        }
        private void Reset()
        {
            ball = SpawnBall();
            player.hit = 0;
            player.miss = 0;
            score = GetScoreString();
            elapsedTime = 0;
        }
        private void InitText()
        {
            //width и hight здесь не используются
            scoreText = new TextOnScreen(GetScoreString());
            scoreText.PutIn(ContentConf.SCORE_X, ContentConf.SCORE_Y);

            gameOverText = new TextOnScreen(ContentConf.GAME_OVER,
                scoreFont.MeasureString(ContentConf.GAME_OVER).X,
                scoreFont.MeasureString(ContentConf.GAME_OVER).Y);
            gameOverText.PutInCenter(GlobalGameVals.WIDTH, GlobalGameVals.HEIGHT);
            gameOverText.MoveY(-(gameOverText.hight));

            playAgainText = new TextOnScreen(ContentConf.PLAY_AGAIN,
                scoreFont.MeasureString(ContentConf.PLAY_AGAIN).X,
                scoreFont.MeasureString(ContentConf.PLAY_AGAIN).Y);
            playAgainText.PutUnder(gameOverText);

            exitText = new TextOnScreen(ContentConf.EXIT,
                scoreFont.MeasureString(ContentConf.EXIT).X,
                scoreFont.MeasureString(ContentConf.EXIT).Y);
            exitText.PutUnder(playAgainText);
        }
        private string GetScoreString()
        {
            return String.Format(ContentConf.SCORE, player.hit, player.miss);
        }
        private Color GetButtonColor(TextOnScreen text)
        {
            if (text.CursorOn)
                return ContentConf.BACKLIGHTED_FONT_COLOR;
            return ContentConf.FONT_COLOR;
        }
        void DrawStringWithOffset(TextOnScreen str, float offsetX, float offsetY)
        {
            Vector2 offsettedPos = new Vector2(str.pos.X + offsetX, str.pos.Y + offsetY);
            spriteBatch.DrawString(scoreFont, str.text, offsettedPos, Color.Black);
            return;
        }
        private void DrawOutlinedString(TextOnScreen str, Color color)
        {
            for (float i = -1; i < 2; i++)
            {
                for (float j = -1; j < 2; j++)
                {
                    DrawStringWithOffset(str, i, j);
                }                    
            }                
            spriteBatch.DrawString(scoreFont, str.text, str.pos, color);
        }
        private bool TimeIsOut()
        {
            return GlobalGameVals.TIME_LIMIT - elapsedTime < 0;
        }            
        private void TheChase(GameTime gameTime)
        {
            elapsedTime += gameTime.ElapsedGameTime.TotalSeconds;
            if (TimeIsOut())
                gameOver = true;

            if (!gameOver)
            {
                bool pressed_now = Mouse.GetState().LeftButton == ButtonState.Pressed;
                if (pressed_now)
                    if (!just_pressed)
                    {
                        just_pressed = true;
                        if (CursorOnBall())
                            ConfirmHit(ball);
                        else
                        {
                            player.miss++;
                            if (player.miss == GlobalGameVals.MISS_LIMIT)
                                gameOver = true;
                        }

                        score = $"HIT:{player.hit} MISS:{player.miss}";
                    }
                    else { /*ничего не происходит*/}
                else
                    just_pressed = false;

                ball.Move();
            }
            else
            {
                if (CursorOnText(playAgainText))
                {
                    playAgainText.CursorOn = true;
                    if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                    {
                        gameOver = false;
                        Reset();
                    }
                }
                else
                    playAgainText.CursorOn = false;

                exitText.CursorOn = CursorOnText(exitText);
                if (exitText.CursorOn)
                {
                    if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                        this.Exit();
                }
            }
        }
        private void TheHunt(GameTime gameTime)
        {
            gameOver = balls.Count == 0;
            if (!gameOver)
            {
                var b = balls.GetEnumerator();
                while (b.MoveNext())
                {
                    if (!LBPressed())
                        just_pressed = false;
                    else
                        if (!just_pressed && CursorOnBall())
                        ConfirmHit(ball);

                }
                balls.RemoveAll(ball => ball.destroyed);
            }
        }
        private void CheckBall(Ball ball)
        {
            bool pressed_now = Mouse.GetState().LeftButton == ButtonState.Pressed;
            if (pressed_now)
                if (!just_pressed)
                {
                    just_pressed = true;

                    else
                    {
                        player.miss++;
                        if (player.miss == GlobalGameVals.MISS_LIMIT)
                            gameOver = true;
                    }

                    //score = $"HIT:{player.hit} MISS:{player.miss}";
                }
                else { /*ничего не происходит*/}
            else
                just_pressed = false;

            ball.Move();
        }
        private bool LBPressed()
        {
            return Mouse.GetState().LeftButton == ButtonState.Pressed;
        }
    }
}
