﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace ClickerDestroyer
{
    public static class ContentConf
    {
        public const float WIDTH = 640;
        public const float HEIGHT = 480;

        public const string GAME_OVER = "GAME OVER";
        public const string PLAY_AGAIN = "PLAY AGAIN";
        public const string EXIT = "EXIT";
        public const string SCORE = "HIT:{0} MISS:{1}";
        public const float SCORE_X = 10;
        public const float SCORE_Y = 10;

        public static Color FONT_COLOR = Color.White;
        public static Color BACKLIGHTED_FONT_COLOR = Color.Yellow;
    }
}
