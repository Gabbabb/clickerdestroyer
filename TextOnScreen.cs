﻿namespace ClickerDestroyer
{
    public class TextOnScreen
    {
        public string text;
        public Microsoft.Xna.Framework.Vector2 pos;
        public float width;
        public float hight;
        private bool cursorOn = false;

        public bool CursorOn
        {
            set
            {
                if (cursorOn && value)
                    return;
                cursorOn = value;
            }
            get { return cursorOn; }
        }

        public TextOnScreen(string text)
        {
            this.text = text;
        }

        public TextOnScreen(string text, float width, float hight)
        {
            this.text = text;
            this.width = width;
            this.hight = hight;
        }

        public void PutInCenter(float screenWidth, float screenHeight)
        {
            pos.X = (screenWidth - width) / 2;
            pos.Y = (screenHeight - hight) / 2;
        }

        public void PutIn(float x, float y)
        {
            pos.X = x;
            pos.Y = y;
        }

        public void PutUnder(TextOnScreen anotherText)
        {
            pos.Y = anotherText.pos.Y + anotherText.hight;
            pos.X = anotherText.pos.X;
        }

        public void MoveY(float d)
        {
            pos.Y += d;
        }

        public void CheckCursor()
        {
            if (!cursorOn)
                cursorOn = true;
        }
    }
}
