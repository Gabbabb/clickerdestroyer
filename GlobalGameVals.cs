﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClickerDestroyer
{
    public enum GameMode { Chase , Hunt};
    static class GlobalGameVals
    {  
        public static GameMode GAME_MODE = GameMode.Chase;

        public const int WIDTH = 640;
        public const int HEIGHT = 480;        
        public static int BALL_WIDTH;
        public static int BALL_HEIGHT;
        
        public static readonly int MISS_LIMIT = 3;
        public static readonly int SPEEDUP_RATE = 2;
        public static readonly int SLOWDOWN_RATE = 1;
        public static readonly double TIME_LIMIT = 5;
        public static readonly double TIME_REWARD = 1;

        public static int LimitXpos()
        {
            return WIDTH - BALL_WIDTH;
        }        
        public static int LimitYpos()
        {
            return HEIGHT - BALL_HEIGHT;
        }
    }
}
